#include "StartScene.h"
USING_NS_CC;

cocos2d::Scene * StartScene::createScene()
{
	return StartScene::create();
}

bool StartScene::init()
{
	configs cfg("levels.json");

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Vector<MenuItem*> MenuItems;
	
	int row = 0;
	int col = 0;

	levels = cfg.getConfigs();

	for (auto lvl : levels)
	{
		std::string str = std::to_string(lvl.first);
		auto label = Label::createWithTTF(str, "fonts/arial.ttf", 50);
		label->setColor(cocos2d::Color3B(0, 0, 0));
		auto menuItem = MenuItemImage::create("button.png", "button.png", CC_CALLBACK_1(StartScene::itemPressed, this));
		float labelX = menuItem->getContentSize().width / 2 - label->getContentSize().width / 2;
		float labelY = menuItem->getContentSize().height / 2 - label->getContentSize().height / 2;;
		label->setPosition(Vec2(labelX, labelY));
		menuItem->addChild(label);
		menuItem->setTag(lvl.first);

		float x = menuItem->getContentSize().width / 2 + ((menuItem->getContentSize().width + 5) * col) + 10;
		float y = visibleSize.height - ((menuItem->getContentSize().height / 2) + ((menuItem->getContentSize().height + 10) * row) + 10);
		
		menuItem->setPosition(Vec2(x, y));
		MenuItems.pushBack(menuItem);
		if ((x + (menuItem->getContentSize().width * 2) + 5) >= visibleSize.width)
		{
			row++;
			col = 0;
		}
		else
		{
			col++;
		}
	}

	auto menu = Menu::createWithArray(MenuItems);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);
	return true;
}

void StartScene::itemPressed(Ref * pSender)
{
	MenuItem* pMenuItem = (MenuItem *)(pSender);
	int tag = (int)pMenuItem->getTag();
	auto newScene = GameScene::createScene(levels.at(tag));
	Director::getInstance()->replaceScene(newScene);
}
