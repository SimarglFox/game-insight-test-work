#ifndef __CONFIGS_H__
#define __CONFIGS_H__

#include "map"
#include "string"
#include "vector"
#include "UserDefinition.h"

class configs
{
public:
	configs(std::string jsonFile);

	std::map<int, levelConfig>& getConfigs();
	~configs();

	int count() { return levels.size() ; }
private:
	std::map<int, levelConfig> levels;
};

#endif