#pragma once
#ifndef __START_SCENE_H__
#define __START_SCENE_H__

#include "configs.h"
#include "../gameScene/GameScene.h"

class StartScene : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init() override;

	// implement the "static create()" method manually
	CREATE_FUNC(StartScene);

	void itemPressed(Ref* pSender);

private:
	std::map<int, levelConfig> levels;
};

#endif