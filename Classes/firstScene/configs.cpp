#include "configs.h"
#include "CCFileUtils.h"
#include "json/reader.h"
#include "json/writer.h"
#include "json/document.h"
#include "json/stringbuffer.h"

USING_NS_CC;

configs::configs(std::string jsonFile)
{
	rapidjson::Document document;

	std::string fileData = FileUtils::getInstance()->getStringFromFile(jsonFile.c_str());
	if (document.Parse<0>(fileData.c_str()).HasParseError())
	{
		Director::getInstance()->end();
	}

	if (document.HasMember("levels"))
	{
		for (int i = 0; i < document["levels"].Size(); i++) {
			int id = document["levels"][i]["id"].GetInt();
			levelConfig cfg;
			cfg.width = document["levels"][i]["width"].GetInt();
			cfg.height = document["levels"][i]["height"].GetInt();
			cfg.scores = document["levels"][i]["scores"].GetInt();
			cfg.goal = document["levels"][i]["goal"].GetInt();
			for (int j = 0; j < document["levels"][i]["gems"].Size(); j++)
			{
				cfg.gems.push_back(document["levels"][i]["gems"][j].GetString());
			}
			levels.insert(std::pair<int, levelConfig>(id, cfg));
		}
	}
}

std::map<int, levelConfig>& configs::getConfigs()
{
	return levels;
}

configs::~configs()
{

}
