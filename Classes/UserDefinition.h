#pragma once
#ifndef __USER_DEFINITION_H__
#define __USER_DEFINITION_H__
#include <vector>
#include <cocos2d.h>
#define WIDTH 1000
#define HEIGHT 650
struct levelConfig
{
	int width;
	int height;
	int scores;
	int goal;
	std::vector<std::string> gems;
};

enum
{
	Board_tag,
	Gem_tag,
	UI_tag
};
#endif