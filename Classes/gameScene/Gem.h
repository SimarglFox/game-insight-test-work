#pragma once
#ifndef __GEM_H__
#define __GEM_H__

#include "cocos2d.h"

class Gem : public cocos2d::Sprite
{
public:
	bool this_is_boom;
	bool needToBoom;
	Gem() { this_is_boom = false; needToBoom = false; };
	~Gem() {};

	std::string gemType;

	static Gem* Gem::create(const std::string gemName);
	cocos2d::Vec2 getArrayPosition();
	void setArrayPosition(cocos2d::Vec2& position);
private:
	cocos2d::Vec2 arrayPosition;
};

#endif