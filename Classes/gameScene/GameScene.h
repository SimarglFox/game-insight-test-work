#pragma once
#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "UserDefinition.h"
#include "GameManager.h"
#include "../firstScene/StartScene.h"
#include "ui/CocosGUI.h"

class GameScene : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene(levelConfig inputLevelConfig);

	virtual bool init() override;
	void CustomInit();
	bool onTouchBegan(Touch *touch, Event *unused_event) override;
	void onTouchMoved(Touch *touch, Event *unused_event) override;
	void onTouchEnded(Touch *touch, Event *unused_event) override; 
	
	void Loop(float dt);
	CREATE_FUNC(GameScene);

private:
	cocos2d::ui::Button* button;
	levelConfig cfg;
	cocos2d::Label* score;
	cocos2d::Label* game_goal; 
	GameManager* gameManager;
	bool execute;
	cocos2d::Vec2 touchPosition;

	bool action_time;
	int cur_score;
	int cur_moves;

	float curTime;
	int time;

};


#endif