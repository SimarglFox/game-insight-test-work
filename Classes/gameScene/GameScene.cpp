﻿#include "GameScene.h"

USING_NS_CC;

cocos2d::Scene * GameScene::createScene(levelConfig inputLevelConfig)
{
	auto scene = Scene::create();
	auto layer = GameScene::create();
	scene->addChild(layer);
	layer->cfg = inputLevelConfig;
	layer->CustomInit();
	return scene;
}

bool GameScene::init()
{
	if (!Layer::init())
		return false;
	true;
}

void GameScene::CustomInit()
{
	this->setTouchEnabled(true);
	this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

	LayerColor* board = LayerColor::create(Color4B(255, 255, 255, 50));
	board->setPosition(Vec2::ZERO);
	board->setContentSize(Size(650, 650));
	board->setName("BOARD");
	this->addChild(board, Board_tag);

	score = Label::create("dd", "fonts/arial.ttf", 60, Size::ZERO, TextHAlignment::CENTER);
	score->setPosition(Vec2((WIDTH / 6) * 5, HEIGHT - 100));
	score->setName("SCORE");
	this->addChild(score);

	auto labelString = StringUtils::format("0/%d", cfg.goal);
	game_goal = Label::create(labelString, "Arial", 100, Size::ZERO, TextHAlignment::CENTER);
	game_goal->setPosition(Vec2((WIDTH / 6) * 5, HEIGHT - 350));
	game_goal->setName("GAME_GOAL");
	this->addChild(game_goal);

	button = cocos2d::ui::Button::create();
	button->setTitleText("back");
	button->setPosition(Vec2((WIDTH / 6) * 5, HEIGHT - 600));

	button->addTouchEventListener([&](Ref* sender, cocos2d::ui::Widget::TouchEventType type) {
		auto newScene = StartScene::createScene();
		Director::getInstance()->replaceScene(newScene);
	});

	this->addChild(button);
	gameManager = new GameManager();
	gameManager->Initialize(board, cfg);

	execute = true;

	action_time = false;
	cur_score = 0;

	curTime = 0;
	time = 30;

	schedule(schedule_selector(GameScene::Loop), 0.0f);
}

void GameScene::Loop(float dt)
{
	if (cur_score <= gameManager->getScore())
	{
		if (!action_time)
		{
			action_time = true;
			score->runAction(
				Sequence::create(
					ScaleTo::create(0.3f, 2.5f),
					ScaleTo::create(0.3f, 1.5f),
					CallFunc::create([=]() {action_time = false; }), nullptr));
		}

		score->setString(StringUtils::format("%d", cur_score));
		cur_score += 1;
	}
	if (cur_moves < gameManager->getMove())
	{
		if (!action_time)
		{
			action_time = true;
			game_goal->runAction(
				Sequence::create(
					ScaleTo::create(0.3f, 2.5f),
					ScaleTo::create(0.3f, 1.5f),
					CallFunc::create([=]() {action_time = false; }), nullptr));
		}

		game_goal->setString(StringUtils::format("%d/%d", gameManager->getMove(), cfg.goal));
		cur_moves = gameManager->getMove();
	}
	if (cur_moves == cfg.goal)
	{
		auto newScene = StartScene::createScene();
		Director::getInstance()->replaceScene(newScene);
	}
	if (cur_score == cfg.scores)
	{
		auto newScene = StartScene::createScene();
		Director::getInstance()->replaceScene(newScene);
	}
}

bool GameScene::onTouchBegan(Touch *touch, Event *unused_event) {
	touchPosition = touch->getLocation();

	if (!execute)
	{
		this->unschedule(schedule_selector(GameScene::Loop));
		
		execute = false;
		return true;
	}

	if (execute)
	{
		gameManager->TouchBegan(touch);
	}
	return true;
}

void GameScene::onTouchMoved(Touch *touch, Event *unused_event) {
	if (execute)
		gameManager->TouchMoved(touch);
}

void GameScene::onTouchEnded(Touch *touch, Event *unused_event) {
	if (execute)
		gameManager->TouchEnded(touch);
}