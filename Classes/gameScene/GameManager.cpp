﻿#include "GameManager.h"


void GameManager::Initialize(Node* _ownerLayer, levelConfig cfg)
{
	srand((unsigned int)time(NULL));
	ownerLayer = _ownerLayer;
	vertical_boom = false;
	horizontal_boom = false;
	pluse_boom = false;
	no_three = false;
	vAllGem.clear();
	vGem.clear();
	dropGem = false;
	first_execute = false;
	three_drop = false;
	is_swap = false;
	boomGem = false;
	is_swap_return = false;
	score = 0;
	move = 0;
	touch_begine = Vec2::ZERO;
	touch_end = Vec2::ZERO;

	lvlCfg = cfg;

	this->ClearBoard();
}

Gem* GameManager::CreateGem(const Vec2& arr_position)
{
	Gem* gem = Gem::create(RandomGem(rand() % lvlCfg.gems.size() + 0));
	gem_size = gem->getContentSize().width;
	gem->setAnchorPoint(Vec2::ZERO);
	gem->setPosition(ArrayToMap(arr_position));
	gem->setArrayPosition(Vec2(arr_position));
	gem->setTag(0);
	ownerLayer->addChild(gem, Gem_tag);

	vAllGem.push_back(gem);

	return gem;
}

std::string GameManager::RandomGem(int n)
{
	return lvlCfg.gems.at(n);
}

Vec2 GameManager::MapToArray(const Vec2& position)
{
	int xpos = position.x / gem_size;
	int ypos = (650 - position.y) / gem_size;
	return Vec2(xpos, ypos);
}

Vec2 GameManager::ArrayToMap(const Vec2& position)
{
	int xpos = position.x * gem_size;
	int ypos = position.y * gem_size;
	return Vec2(xpos, ypos);
}

void GameManager::ClearBoard()
{
	first_execute = true;
	for (int i = 0; i < lvlCfg.height; i++) {
		std::vector<Gem*> lines;
		for (int j = 0; j < lvlCfg.width; j++)
		{
			Gem* newGem = CreateGem(Vec2(j, i));
			lines.push_back(newGem);
		}
		gemBoard.push_back(lines);
	}

	for (int i = 0; i < lvlCfg.height; i++) {
		for (int j = 0; j < lvlCfg.width; j++) {
			std::vector<Vec2> dir = { Vec2(1, 0), Vec2(0, 1) };

			int x = gemBoard[i][j]->getArrayPosition().x;
			int y = gemBoard[i][j]->getArrayPosition().y;

			Gem* curGem = gemBoard[y][x];
			for (auto it : dir) {
				while ((CheckGem(x, y, it) + CheckGem(x, y, it * - 1) + 1) >= 2)
				{
						vAllGem.erase(std::find(vAllGem.begin(), vAllGem.end(), gemBoard[y][x]));
						ownerLayer->removeChild(gemBoard[y][x], true);

						gemBoard[y][x] = nullptr;

						Gem* newGem = CreateGem(Vec2(x, y));
						gemBoard[y][x] = newGem;
				}
			}
		}
	}
	first_execute = false;
}

void GameManager::TouchBegan(Touch* _touch)
{
	if (!boomGem) {
		if (!dropGem) {
			if (!pluse_boom) {
				if (!is_swap) {

					touch_begine = _touch->getLocationInView();
					touch_end = _touch->getLocationInView();
					Vec2 temp = MapToArray(touch_begine);
					if (temp.y > lvlCfg.height || temp.x > lvlCfg.width)
					{
						return;
					}
					no_three = false;

					is_begine_touch_enable = true;

					if (gemBoard[(int)temp.y][(int)temp.x]->needToBoom == true)
					{
						vBoomGem.clear();

						PluseBoom(temp.x, temp.y, Vec2(1, 0));
						PluseBoom(temp.x, temp.y, Vec2(1, 0)* -1);
						PluseBoom(temp.x, temp.y, Vec2(0, 1));
						PluseBoom(temp.x, temp.y, Vec2(0, 1)*-1);

						boomGem = true;
						dropGem = true;
						pluse_boom = true;
						is_swap = true;

						vBoomGem.push_back(gemBoard[(int)temp.y][(int)temp.x]);
						
						std::vector<int> ARR_Y(lvlCfg.width, lvlCfg.width);
						for (auto it : vBoomGem)
						{
							three_drop = true;

							int arr_x = (int)it->getArrayPosition().x;

							ARR_Y[arr_x] += 1;
							Gem* gem = CreateGem(Vec2(arr_x, ARR_Y[arr_x] - 1));
						}

						for (Gem* bGem : vBoomGem) {
							for (Gem* aGem : vAllGem) {
								if ((aGem->getArrayPosition().x == bGem->getArrayPosition().x) &&
									(aGem->getArrayPosition().y > bGem->getArrayPosition().y)) {
									aGem->setTag(aGem->getTag() + 1);
								}
							}
						}

						for (Gem* bGem : vBoomGem) {
							vAllGem.erase(std::find(vAllGem.begin(), vAllGem.end(), bGem));
							ownerLayer->removeChild(bGem, true);
							score += 1;
						}

						int max_num = 1;

						for (Gem* gem : vAllGem)
						{
							if (gem->getTag() >= max_num)
								max_num = gem->getTag();

							if (gem)
							{
								gem->runAction(MoveBy::create(0.2f * gem->getTag(), Vec2(0, -1 * (gem->getTag()) * gem_size)));
								gem->setArrayPosition(Vec2(gem->getArrayPosition() - Vec2(0, gem->getTag())));
								gemBoard[(int)gem->getArrayPosition().y][(int)gem->getArrayPosition().x] = gem;
								gem->setTag(0);
							}
						}

						vertical_boom = false;
						horizontal_boom = false;

						ownerLayer->runAction(
							Sequence::create(
								DelayTime::create(0.2f * max_num),
								CallFuncN::create(
									[=](Node* gem)
						{
							boomGem = false;
							dropGem = false;
							pluse_boom = false;
							is_swap = false;

							DropAndCheck();
						}), nullptr));
					}
				}
			}
		}
	}
}

void GameManager::TouchMoved(Touch* _touch) {
	if (!boomGem) {
		if (is_begine_touch_enable) {
			if (!dropGem) {
				if (!pluse_boom) {
					if (!is_swap)
					{
						touch_end = _touch->getLocationInView();
					}
				}
			}
		}
	}
}

void GameManager::TouchEnded(Touch* _touch)
{
	if (is_begine_touch_enable) {
		if (!boomGem) {
			if (!dropGem) {
				if (!pluse_boom) {
					if (!is_swap) {
						vBoomGem.clear();
						Vec2 temp = touch_end - touch_begine;
						Vec2 arr_dir;
						if (abs(temp.x) < abs(temp.y))
						{
							if (temp.y >= 20.0f)
								arr_dir = Vec2(0, -1);
							else if (temp.y <= -20.0f)
								arr_dir = Vec2(0, 1);
						}
						else 
						{
							if (temp.x >= 20.0f)
								arr_dir = Vec2(1, 0);
							else if (temp.x <= -20.0f)
								arr_dir = Vec2(-1, 0);
						}


						Vec2 sel = MapToArray(touch_begine);
						if (gemBoard[(int)sel.y][(int)sel.x] == nullptr)
						{
							return;
						}
						Gem* selected = gemBoard[(int)sel.y][(int)sel.x];

						Vec2 tar = sel + arr_dir;

						if (sel.x < 0 || sel.x >= lvlCfg.width || sel.y < 0 || sel.y >= lvlCfg.height)
							return;
						if (tar.x < 0 || tar.x >= lvlCfg.width || tar.y < 0 || tar.y >= lvlCfg.height)
							return;

						Gem* target = gemBoard[(int)tar.y][(int)tar.x];
						if (!selected || !target)
							return;


						SwapGem(selected, target);
					}
				}
			}
		}
	}
}

void GameManager::SwapGem(Gem* target1, Gem* target2)
{
	if (!boomGem) {
		if (!pluse_boom) {
			if (!dropGem) {
				if (!is_swap) {

					if (!target1 || !target2)
						return;

					if (target1)
						target1->stopAllActions();
					if (target2)
						target2->stopAllActions();

					Vec2 target1_position = ArrayToMap(target1->getArrayPosition());
					Vec2 target2_position = ArrayToMap(target2->getArrayPosition());

					Vec2 arr_target1_position = target1->getArrayPosition();
					Vec2 arr_target2_position = target2->getArrayPosition();

					auto action1 = MoveTo::create(0.2f, target2_position);
					auto action2 = MoveTo::create(0.2f, target1_position);

					target1->runAction(Sequence::create(action1->clone(), CallFunc::create([=]() {}), nullptr));
					target2->runAction(Sequence::create(action2->clone(), CallFunc::create([=]() {}), nullptr));

					target1->setArrayPosition(arr_target2_position);
					target2->setArrayPosition(arr_target1_position);

					gemBoard[(int)arr_target1_position.y][(int)arr_target1_position.x] = target2;
					gemBoard[(int)arr_target2_position.y][(int)arr_target2_position.x] = target1;

					if (!is_swap) {
						is_swap = true;
						ownerLayer->runAction(Sequence::create(DelayTime::create(0.2f), CallFunc::create([=]()
						{
							if (is_swap)
							{
								is_swap = false;
								if (!boomGem)
								{
									if (!dropGem)
									{
										if (is_swap_return)
										{
											is_swap = false;
											vBoomGem.clear();
											is_swap_return = false;
											is_begine_touch_enable = false;
										}
										else
										{
											move++;
											vGem.clear();
											vBoomGem.clear();
											if (SwapGemCheck(target1) || SwapGemCheck(target2))
											{
												is_swap = false;
												is_begine_touch_enable = true;
												three_drop = false;

												touch_begine = Vec2::ZERO - Vec2(100, 100);
												touch_end = Vec2::ZERO - Vec2(100, 100);

												BoomBoomBoom(target1, target2);
											}
											else if (!no_three)
											{
												if (!boomGem)
												{
													if (!dropGem)
													{
														if (!is_swap_return)
															is_swap_return = true;
														is_swap = false;
														no_three = true;
														target1->stopAllActions();
														target2->stopAllActions();
														SwapGem(target1, target2);
													}
												}
											}
										}
									}
								}
							}
						}), nullptr));
					}
				}
			}
		}
	}
}

int GameManager::CheckGem(int arr_x, int arr_y, const Vec2& dir)
{
	int arr_next_x = arr_x + dir.x;
	int arr_next_y = arr_y + dir.y;

	if (arr_next_x < 0 || arr_next_x >= lvlCfg.width || arr_next_y < 0 || arr_next_y >= lvlCfg.height)
		return 0;
	if (gemBoard[arr_y][arr_x] == nullptr || gemBoard[arr_next_y][arr_next_x] == nullptr)
		return 0;
	if (gemBoard[arr_y][arr_x]->gemType != gemBoard[arr_next_y][arr_next_x]->gemType)
		return 0;

	if (!first_execute) {
		vGem.push_back(gemBoard[arr_next_y][arr_next_x]);
	}
	return 1 + CheckGem(arr_next_x, arr_next_y, dir);
}

int GameManager::BoomCheck(int arr_x, int arr_y, const Vec2& dir)
{
	int arr_next_x = arr_x + dir.x;
	int arr_next_y = arr_y + dir.y;

	if (arr_next_x < 0 || arr_next_x >= lvlCfg.width || arr_next_y < 0 || arr_next_y >= lvlCfg.width)
		return 0;
	if (gemBoard[arr_y][arr_x] == nullptr || gemBoard[arr_next_y][arr_next_x] == nullptr)
		return 0;
	if (gemBoard[arr_y][arr_x]->gemType != gemBoard[arr_next_y][arr_next_x]->gemType)
		return 0;

	return 1 + CheckGem(arr_next_x, arr_next_y, dir);
}

int GameManager::PluseBoom(int arr_x, int arr_y, const Vec2& dir)
{
	int arr_next_x = arr_x + dir.x;
	int arr_next_y = arr_y + dir.y;

	if (arr_next_x < 0 || arr_next_x >= lvlCfg.width || arr_next_y < 0 || arr_next_y >= lvlCfg.width)
		return 0;
	if (gemBoard[arr_y][arr_x] == nullptr || gemBoard[arr_next_y][arr_next_x] == nullptr)
		return 0;

	auto boomIter = std::find_if(vBoomGem.begin(), vBoomGem.end(), [=](Gem* gem)->bool
	{
		if (gemBoard[arr_next_y][arr_next_x]->getArrayPosition() == gem->getArrayPosition())
			return true;
		else
			return false;
	});

	if (boomIter != vBoomGem.end())
		log("overlap");
	else
		vBoomGem.push_back(gemBoard[arr_next_y][arr_next_x]);


	return 1 + PluseBoom(arr_next_x, arr_next_y, dir);
}

bool GameManager::SwapGemCheck(Gem* gem)
{
	int arr_x = gem->getArrayPosition().x;
	int arr_y = gem->getArrayPosition().y;

	int vertical = 1;
	int horizontal = 1;
	bool debug = false;

	vGem.clear();
	vertical += CheckGem(arr_x, arr_y, Vec2(0, 1));
	vertical += CheckGem(arr_x, arr_y, Vec2(0, 1)*-1);

	if (vertical > 2) {
		for (auto it : vGem) {
			auto boomIter = std::find_if(vBoomGem.begin(), vBoomGem.end(), [=](Gem* gem)->bool {
				if (it->getArrayPosition() == gem->getArrayPosition())return true; else return false; });
			if (boomIter != vBoomGem.end())
				debug = true;
			else {
				no_three = true;
				vBoomGem.push_back(it);
			}
		}
		vertical_boom = true;
		vGem.clear();
	}
	else if (vertical == 2) {
		for (auto it : vGem) {
			auto boomIter = std::find_if(vBoomGem.begin(), vBoomGem.end(), [=](Gem* gem)->bool {
				if (it->getArrayPosition() == gem->getArrayPosition())return true; else return false; });
			if (boomIter != vBoomGem.end())
				debug = true;
			else {
				no_three = true;
				vBoomGem.push_back(it);
			}
		}
		vGem.clear();
	}

	vGem.clear();
	horizontal += CheckGem(arr_x, arr_y, Vec2(1, 0));
	horizontal += CheckGem(arr_x, arr_y, Vec2(1, 0)*-1);

	if (horizontal > 2) {
		for (auto it : vGem) {
			auto boomIter = std::find_if(vBoomGem.begin(), vBoomGem.end(), [=](Gem* gem)->bool {
				if (it->getArrayPosition() == gem->getArrayPosition())return true; else return false; });
			if (boomIter != vBoomGem.end())
				debug = true;
			else {
				no_three = true;
				vBoomGem.push_back(it);
			}
		}
		vGem.clear();
		horizontal_boom = true;
	}
	else if (horizontal == 2) {
		for (auto it : vGem) {
			auto boomIter = std::find_if(vBoomGem.begin(), vBoomGem.end(), [=](Gem* gem)->bool {
				if (it->getArrayPosition() == gem->getArrayPosition())return true; else return false; });
			if (boomIter != vBoomGem.end())
				debug = true;
			else {
				no_three = true;
				vBoomGem.push_back(it);
			}
		}
		vGem.clear();
	}

	if (vertical >= 2 || horizontal >= 2)
	{
		if (!debug)
		{
			vBoomGem.push_back(gem);
		}
		return true;
	}
	return false;
}

void GameManager::DropAndCheck()
{
	if (!is_swap) {
		if (!boomGem) {
			if (!dropGem) {
				if (!pluse_boom) {
					vBoomGem.clear();
					std::vector<Vec2> dir = { Vec2(1, 0), Vec2(0, 1) };
					for (auto aV : vAllGem)
					{
						SwapGemCheck(aV);
					}

					if (vBoomGem.size() >= 2) {
						auto vIter = vBoomGem.begin();
						for (; vIter != vBoomGem.end();)
						{
							int x = (*vIter)->getArrayPosition().x;
							int y = (*vIter)->getArrayPosition().y;

							if (vertical_boom || horizontal_boom)
							{
								if (vertical_boom) {
									if (BoomCheck((*vIter)->getArrayPosition().x, (*vIter)->getArrayPosition().x, Vec2(0, 1)) +
										BoomCheck((*vIter)->getArrayPosition().x, (*vIter)->getArrayPosition().y, Vec2(0, 1) * -1) + 1 >= 2)
									{
										(*vIter)->needToBoom = true;
										vIter = vBoomGem.erase(vIter);
										break;
									}
								}
								else if (horizontal_boom)
								{
									if (BoomCheck((*vIter)->getArrayPosition().x, (*vIter)->getArrayPosition().x, Vec2(1, 0)) +
										BoomCheck((*vIter)->getArrayPosition().x, (*vIter)->getArrayPosition().y, Vec2(1, 0) * -1) + 1 >= 2)
									{
										(*vIter)->needToBoom = true;
										vIter = vBoomGem.erase(vIter);
										break;
									}
								}

							}
							vIter++;
						}
					}

					std::vector<int> ARR_Y(lvlCfg.width, lvlCfg.width);
					for (auto it : vBoomGem)
					{
						three_drop = true;

						int arr_x = (int)it->getArrayPosition().x;
						int arr_y = (int)it->getArrayPosition().y;


						ARR_Y[arr_x] += 1;
						Gem* newGem = CreateGem(Vec2(arr_x, ARR_Y[arr_x] - 1));
					}

					for (Gem* bGem : vBoomGem) {
						for (Gem* aGem : vAllGem) {
							if ((aGem->getArrayPosition().x == bGem->getArrayPosition().x) &&
								(aGem->getArrayPosition().y > bGem->getArrayPosition().y)) {
								aGem->setTag(aGem->getTag() + 1);
							}
						}
					}

					for (Gem* bGem : vBoomGem) {
						vAllGem.erase(std::find(vAllGem.begin(), vAllGem.end(), bGem));
						ownerLayer->removeChild(bGem, true);
						score += 1;
					}

					int max_num = 0;
					for (Gem* gem : vAllGem) {
						if (gem->getTag() >= max_num)
							max_num = gem->getTag();

						if (gem)
						{
							gem->runAction(MoveBy::create(0.2f * gem->getTag(), Vec2(0, -1 * (gem->getTag()) * gem_size)));
							gem->setArrayPosition(Vec2(gem->getArrayPosition() - Vec2(0, gem->getTag())));
							gemBoard[(int)gem->getArrayPosition().y][(int)gem->getArrayPosition().x] = gem;
							gem->setTag(0);
						}
					}
					if (max_num == 0)
						max_num += 1;

					vertical_boom = false;
					horizontal_boom = false;

					ownerLayer->runAction(Sequence::create(DelayTime::create(0.2f * max_num),
						CallFuncN::create([=](Node* gem)
					{
						dropGem = false;
						three_drop = false;

						DropAndCheck();

					}), nullptr));
				}
			}
		}
	}
}

void GameManager::BoomBoomBoom(Gem* target1, Gem* target2)
{
	if (!is_swap) {
		if (!boomGem) {
			if (!dropGem) {
				if (!three_drop) {
					if (!pluse_boom) {
						vBoomGem.clear();

						vertical_boom = false;
						horizontal_boom = false;

						if (SwapGemCheck(target1) || SwapGemCheck(target2))
						{
							three_drop = true;
							boomGem = true;
						}

						if (horizontal_boom)
						{
							int target1_num = BoomCheck(target1->getArrayPosition().x, target1->getArrayPosition().y, Vec2(1, 0)) +
								BoomCheck(target1->getArrayPosition().x, target1->getArrayPosition().y, Vec2(1, 0) * -1) + 1;

							int target2_num = BoomCheck(target2->getArrayPosition().x, target2->getArrayPosition().y, Vec2(1, 0)) +
								BoomCheck(target2->getArrayPosition().x, target2->getArrayPosition().y, Vec2(1, 0) * -1) + 1;

							if (target1_num > 2)
							{
								target1->needToBoom = true;
								vBoomGem.erase(std::find(vBoomGem.begin(), vBoomGem.end(), target1));
							}
							else if (target2_num > 2)
							{
								target2->needToBoom = true;
								vBoomGem.erase(std::find(vBoomGem.begin(), vBoomGem.end(), target2));
							}
						}
						else if (vertical_boom)
						{
							int target1_num = BoomCheck(target1->getArrayPosition().x, target1->getArrayPosition().y, Vec2(0, 1)) +
								BoomCheck(target1->getArrayPosition().x, target1->getArrayPosition().y, Vec2(0, 1) * -1) + 1;

							int target2_num = BoomCheck(target2->getArrayPosition().x, target2->getArrayPosition().y, Vec2(0, 1)) +
								BoomCheck(target2->getArrayPosition().x, target2->getArrayPosition().y, Vec2(0, 1) * -1) + 1;

							if (target1_num > 2)
							{
								target1->needToBoom = true;
								vBoomGem.erase(std::find(vBoomGem.begin(), vBoomGem.end(), target1));
							}

							if (target2_num > 2)
							{
								target2->needToBoom = true;
								vBoomGem.erase(std::find(vBoomGem.begin(), vBoomGem.end(), target2));
							}
						}

						if (vBoomGem.size()) {
							
							std::vector<int> ARR_Y(lvlCfg.width, lvlCfg.width);
							auto vBoomIter = vBoomGem.begin();

							vBoomIter = vBoomGem.begin();
							for (; vBoomIter != vBoomGem.end();)
							{
								int arr_x = (int)(*vBoomIter)->getArrayPosition().x;
								int arr_y = (int)(*vBoomIter)->getArrayPosition().y;

								ARR_Y[arr_x] += 1;
								Gem* newGem = CreateGem(Vec2(arr_x, ARR_Y[arr_x] - 1));
								vBoomIter++;
							}

							for (Gem* bGem : vBoomGem) {
								for (Gem* aGem : vAllGem) {
									if ((aGem->getArrayPosition().x == bGem->getArrayPosition().x) &&
										(aGem->getArrayPosition().y > bGem->getArrayPosition().y)) {
										aGem->setTag(aGem->getTag() + 1);
										if (vertical_boom || horizontal_boom) {
											aGem->setTag(aGem->getTag());
										}
									}
								}
							}

							ownerLayer->runAction(Sequence::create(DelayTime::create(0.2),
								CallFunc::create([=]() {

								for (Gem* bGem : vBoomGem) {
									if (bGem) {
										vAllGem.erase(std::find(vAllGem.begin(), vAllGem.end(), bGem));
										ownerLayer->removeChild(bGem, true);
										score += 1;
									}
								}

								int max_num = 0;
								for (Gem* gem : vAllGem) {
									if (gem->getTag() >= max_num)
										max_num = gem->getTag();

									gem->runAction(MoveBy::create(0.2f*gem->getTag(), Vec2(0, -1 * (gem->getTag()) * gem_size)));
									gem->setArrayPosition(Vec2(gem->getArrayPosition() - Vec2(0, gem->getTag())));
									gemBoard[(int)gem->getArrayPosition().y][(int)gem->getArrayPosition().x] = gem;
									gem->setTag(0);
								}

								if (max_num == 0)
									max_num += 1;

								vertical_boom = false;
								horizontal_boom = false;

								ownerLayer->runAction(Sequence::create(DelayTime::create(0.2f * max_num),
									CallFuncN::create([=](Node* gem)
								{
									three_drop = false;
									boomGem = false;
									DropAndCheck();
								}), nullptr));

							}), nullptr));
						}
					}
				}
			}
		}
	}
}