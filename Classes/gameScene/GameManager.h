﻿#pragma once
#include "cocos2d.h"
#include "UserDefinition.h"
#include "Gem.h"

USING_NS_CC;
class GameManager
{
public:
	void Initialize(Node* _ownerLayer, levelConfig cfg);
	void TouchBegan(Touch* _touch);
	void TouchMoved(Touch* _touch);
	void TouchEnded(Touch* _touch);

	int getScore() { return score; };
	int getMove() { return move; }
private:
	Node* ownerLayer;
private:
	int move;
	std::vector<std::vector<Gem*>> gemBoard;
	float gem_size;
	Vec2 touch_begine, touch_end;
	bool vertical_boom, horizontal_boom;
	bool pluse_boom;
	bool no_three;
	bool first_execute;
	bool dropGem;
	bool three_drop;

	bool boomGem;

	bool is_swap_return;
	bool is_swap;

	bool is_begine_touch_enable;

	int score;

	Gem* boomberGem;

	levelConfig lvlCfg;
private:
	Gem* CreateGem(const Vec2& arr_position);
	std::string RandomGem(int n);
	void ClearBoard();
	Vec2 MapToArray(const Vec2& position);
	Vec2 ArrayToMap(const Vec2& position);
	void SwapGem(Gem* target1, Gem* target2);
	bool SwapGemCheck(Gem* candy);
	int  BoomCheck(int arr_x, int arr_y, const Vec2& dir);
	int  CheckGem(int arr_x, int arr_y, const Vec2& dir);
	int  PluseBoom(int arr_x, int arr_y, const Vec2& dir);
	void DropAndCheck();
	void BoomBoomBoom(Gem* target1, Gem* target2);
	std::vector<Gem*> vAllGem;
	std::vector<Gem*> vGem;
	std::vector<Gem*> vBoomGem;
};